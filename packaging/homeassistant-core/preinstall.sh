#!/bin/sh

set -e

getent passwd homeassistant >/dev/null 2>&1 || adduser \
    --system \
    --shell /bin/bash \
    --gecos 'Home Assistant' \
    --group \
    --disabled-password \
    --home /var/lib/homeassistant \
    homeassistant
