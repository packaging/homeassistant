#!/bin/sh

find /opt/homeassistant -type d -name __pycache__ -exec rm -rf {} \+
dpkg -S /opt/homeassistant/lib/python${python}/site-packages/* 2>&1 | grep 'dpkg-query: no path found matching pattern' | awk '{print $NF}' | xargs rm -rf
