#!/bin/ash
# shellcheck shell=dash

set -e

# shellcheck disable=SC3010
if [[ -n "${DEBUG}" ]]; then
    set -x
fi

cleanup_error() {
  docker rm -f "homeassistant-core-builder-${CI_JOB_ID}"
  exit 1
}

docker rm -f "homeassistant-core-builder-${CI_JOB_ID}" || true
DOCKER_DEFAULT_PLATFORM="linux/${ARCH}" docker create --name "homeassistant-core-builder-${CI_JOB_ID}" \
  -e CI_PROJECT_DIR=/build \
  -e CI_COMMIT_TAG="${CI_COMMIT_TAG}" \
  -e CI_JOB_NAME="${CI_JOB_NAME}" \
  -e CACHE_REMOTE_TGZ="${CACHE_REMOTE_TGZ}" \
  -e DEBUG="${DEBUG}" \
  registry.gitlab.com/packaging/homeassistant-core/builder:"${CODENAME}" /build/package.sh
docker cp "${CI_PROJECT_DIR}" "homeassistant-core-builder-${CI_JOB_ID}":/build || cleanup_error
docker start "homeassistant-core-builder-${CI_JOB_ID}" || cleanup_error
docker attach "homeassistant-core-builder-${CI_JOB_ID}" || cleanup_error
docker cp "homeassistant-core-builder-${CI_JOB_ID}":/build /tmp/
loop=0
while docker inspect "homeassistant-core-builder-${CI_JOB_ID}" >/dev/null 2>&1
do
    if [ "${loop}" -gt 10 ]; then
        break
    fi
    loop="$((loop+1))"
    docker rm -f "homeassistant-core-builder-${CI_JOB_ID}" >/dev/null 2>&1 || true
done
cp -rv /tmp/build/"${CI_JOB_NAME}"* "${CI_PROJECT_DIR}"/
cp -r /tmp/build/.cache "${CI_PROJECT_DIR}"/
find "${CI_PROJECT_DIR}"/ -type f \( -name '*.deb' -a ! -name "*${CI_COMMIT_TAG}*" \) -delete
