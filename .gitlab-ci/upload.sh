#!/bin/bash

pip3 -qqq install cloudsmith-cli
for CODENAME in noble
do
    find \
        ./package-"${CODENAME}"-* \
        -type f \
        \( ! -name "*_all.deb" -a -name "*${CI_COMMIT_TAG}*.deb" \) \
        | sort -u \
        | xargs -L1 -r cloudsmith push deb morph027/homeassistant-core/ubuntu/"${CODENAME}"
    find \
        ./package-"${CODENAME}"-* \
        -type f \
        -name "*${CI_COMMIT_TAG}*_all.deb" \
        | sort -u -t '/' -k 3 \
        | xargs -L1 -r cloudsmith push deb morph027/homeassistant-core/ubuntu/"${CODENAME}"
done
