[[_TOC_]]

# [Home Assistant](https://www.home-assistant.io/) Core Packages

## What it is

[Home Assistant Core](https://www.home-assistant.io/installation/) installation but as apt repo with ready to use packages (e.g. for automatic upgrades).

## Support

If you like what i'm doing, you can support me via [Paypal](https://paypal.me/morph027), [Ko-Fi](https://ko-fi.com/morph027) or [Patreon](https://www.patreon.com/morph027).

## Requirements

### Python

As of 2025.2, Home Assistant requires Python 3.13 (e.g. via [deadsnakes PPA](https://launchpad.net/~deadsnakes/+archive/ubuntu/ppa)).

## Add repo

Package repository hosting is graciously provided by  [Cloudsmith](https://cloudsmith.com).
Cloudsmith is the only fully hosted, cloud-native, universal package management solution, that
enables your organization to create, store and share packages in any format, to any place, with total
confidence.

```bash
curl -1sLf 'https://dl.cloudsmith.io/public/morph027/homeassistant-core/gpg.EBED15E28E10F0BA.key' | gpg --dearmor >/usr/share/keyrings/morph027-homeassistant-core-archive-keyring.gpg
. /etc/lsb-release
cat >/etc/apt/sources.list.d/morph027-homeassistant-core.sources <<EOF
Enabled: yes
Types: deb
URIs: https://dl.cloudsmith.io/public/morph027/homeassistant-core/deb/ubuntu
Suites: ${DISTRIB_CODENAME}
Components: main
Signed-By: /usr/share/keyrings/morph027-homeassistant-core-archive-keyring.gpg
EOF
```

Other methods, see https://cloudsmith.io/~morph027/repos/homeassistant-core/setup/#formats-deb

## Run as service using systemd

* install `homeassistant-core-systemd`
* enable and start service: `systemctl enable --now homeassistant-core.service`

## Automatic updates using unattended-upgrades

```bash
echo 'Unattended-Upgrade::Allowed-Origins {"cloudsmith/morph027/homeassistant-core:${distro_codename}";};' | sudo tee /etc/apt/apt.conf.d/50homeassistant-core
```
