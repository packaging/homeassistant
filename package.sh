#!/bin/bash

set -e

if [[ -n "${DEBUG}" ]]; then
    set -x
fi

export DEBIAN_FRONTEND=noninteractive
script_dir="$(dirname "$(readlink -f "$0")")"
python="${PYTHON:-3.13}"
tag="${CI_COMMIT_TAG}"
version="${tag%%+*}"
architecture="$(dpkg --print-architecture)"
eval "$(dpkg-architecture -s)"

# shellcheck disable=SC1091
. /etc/lsb-release

function quiet() {
    if [[ -n "${DEBUG}" ]]; then
        # shellcheck disable=SC2048
        $*
    else
        local log
        log="$(mktemp)"
        # shellcheck disable=SC2048
        if ! $* &>"${log}"; then
            cat "${log}"
            exit $?
        fi
    fi
}

function info() {
    printf "\e[1;36m%s\e[0m\n" "$*"
}

function error() {
    printf "\e[1;31m%s\e[0m\n" "$*"
    exit 1
}

function debug() {
    if [[ -n "${DEBUG}" ]]; then
        printf "\e[0;33m%s\e[0m\n" "$@"
    fi
}

info "Preparing caches ..."
cache_dir="${CI_PROJECT_DIR:-/tmp}/.cache"
debug "apt packages 1"
quiet apt-get update
quiet apt-get -y install \
    curl
if [[ -n "${CACHE_REMOTE_TGZ}" ]] && [ ! -d "${cache_dir}" ]; then
    debug "fetching remote cache from ${CACHE_REMOTE_TGZ}"
    curl -sfLo /tmp/cache.tgz "${CACHE_REMOTE_TGZ}"
    tar -C "${cache_dir%/*}" -xzf /tmp/cache.tgz
    if [ ! -d "${cache_dir}" ]; then
        error "${CACHE_REMOTE_TGZ}: wrong format"
    fi
fi
export PIP_CACHE_DIR="${cache_dir}/pip"
export CCACHE_DIR="${cache_dir}/ccache"
export CCACHE_NAMESPACE="homeassistant_${architecture}"
export UV_CACHE_DIR="${cache_dir}/uv"
export CARGO_HOME="${cache_dir}/cargo-${architecture}"
export UV_LINK_MODE="copy"
export PATH="/usr/lib/ccache:${PATH}"
(
    echo "Dir::State \"${cache_dir#*/}/apt\";"
    echo "Dir::Cache{Archives ${cache_dir}/apt/}"
    echo "Dir::Cache::Archives ${cache_dir}/apt;"
) >/etc/apt/apt.conf.d/01cache
mkdir -p "${CI_PROJECT_DIR:-/tmp}/.cache/apt" "${CCACHE_DIR}"
ln -sf /var/lib/dpkg "${cache_dir}"/

info "Installing toolchain ..."
debug "apt packages 2"
echo "deb [trusted=yes] https://ppa.launchpadcontent.net/deadsnakes/ppa/ubuntu ${DISTRIB_CODENAME} main" >/etc/apt/sources.list.d/deadsnakes-ubuntu-ppa-noble.list
quiet apt-get update
quiet apt-get -y install \
    autoconf \
    binutils \
    ccache \
    curl \
    dpkg-dev \
    gettext-base \
    gcc \
    g++ \
    git \
    libmysqlclient-dev \
    libpcap-dev \
    libpq-dev \
    liblzma-dev \
    python"${python}" \
    python"${python}"-dev \
    python"${python}"-venv \
    zstd

if [ ! -f "${CARGO_HOME}"/env ]; then
    debug "rust"
    quiet curl -f --proto '=https' --tlsv1.2 -LsSf -o /tmp/rust-installer.sh https://sh.rustup.rs
    quiet sh /tmp/rust-installer.sh -y
fi
# shellcheck disable=SC1090,SC1091
source "${CARGO_HOME}"/env

if [ ! -f ~/.local/bin/env ]; then
    debug "uv"
    quiet curl -f --proto '=https' --tlsv1.2 -LsSf -o /tmp/uv-installer.sh "https://github.com/astral-sh/uv/releases/download/${UV:-0.5.4}/uv-installer.sh"
    quiet sh /tmp/uv-installer.sh
fi
# shellcheck disable=SC1090,SC1091
source ~/.local/bin/env

debug "uv venv"
quiet uv ${DEBUG:+-v} venv --python "${python}" /opt/homeassistant
# shellcheck disable=SC1091
. /opt/homeassistant/bin/activate

if [[ -n "${CI}" ]]; then
    rm -rf core
fi

if [ ! -d core ]; then
    info "Fetching home-assistant core ${version} ..."
    quiet git clone -b "${version}" --depth 1 https://github.com/home-assistant/core
fi

debug "Adjusting pip constraints location"
sed -i \
    -e "s,-c homeassistant/package_constraints.txt,-c ${PWD}/core/homeassistant/package_constraints.txt," \
    core/requirements.txt

cd core
python -m script.translations develop --all
cd -

info "Creating package homeassistant-core ${tag} ..."

debug "Adding extra packages to requirements"
# grep -o "install of.*" /var/lib/homeassistant/.homeassistant/home-assistant.log | cut -d' ' -f3
{
    echo "aiohttp-fast-zlib[isal]"
    echo "homeassistant @ file://localhost/${PWD}/core"
    echo "mysqlclient"
    echo "psycopg2"
} >>./core/requirements_all.txt

uv ${DEBUG:+-v} pip install --refresh -r ./core/requirements_all.txt

debug "Testing installation ..."
python"${python}" -c "import speex_noise_cpp"

if ! command -V nfpm >/dev/null 2>&1; then
    nfpm_version="${NFPM_VERSION:-2.40.0}"
    curl -sfLo "${tmpdir}/nfpm.deb" "https://github.com/goreleaser/nfpm/releases/download/v${nfpm_version}/nfpm_${nfpm_version}_${DEB_HOST_ARCH}.deb"
    apt-get -qqy install "${tmpdir}/nfpm.deb"
fi
rm -f "${tmpdir}/nfpm.deb"

mkdir -p "${script_dir}/${CI_JOB_NAME:-${DISTRIB_CODENAME}}"

tmpdir="$(mktemp -d)"

mkdir -p "${tmpdir}/etc/default"

cp "${script_dir}"/packaging/homeassistant-core-systemd/homeassistant-core.env "${tmpdir}/etc/default"

envsubst >"${tmpdir}"/homeassistant-core.postremove.sh <"${script_dir}"/packaging/homeassistant-core/postremove.sh

cat >"${tmpdir}/homeassistant-core.yml" <<EOF
name: homeassistant-core
provides:
  - homeassistant
deb:
  compression: zstd
section: misc
license: MIT
arch: ${architecture}
version: ${tag}~${DISTRIB_ID,,}${DISTRIB_RELEASE}
version_schema: none
maintainer: "Stefan Heitmüller <stefan.heitmueller@gmx.com>"
description: Open source home automation that puts local control and privacy first.
homepage: https://gitlab.com/packaging/homeassistant-core
depends:
  - adduser
  - gcc
  - g++
  - isal
  - python${python}
  - tzdata
recommends:
  - libturbojpeg
  - python${python}-dev
suggests:
  - ffmpeg
  - libpcap-dev
contents:
  - src: /opt/homeassistant
    dst: /opt/homeassistant
    type: tree
    file_info:
      owner: homeassistant
      group: homeassistant
scripts:
  preinstall: ${script_dir}/packaging/homeassistant-core/preinstall.sh
  postremove: ${tmpdir}/homeassistant-core.postremove.sh
EOF
nfpm package --config "${tmpdir}/homeassistant-core.yml" --packager deb

envsubst >"${tmpdir}"/homeassistant-core.service <"${script_dir}"/packaging/homeassistant-core-systemd/homeassistant-core.service

cat >"${tmpdir}/homeassistant-core-systemd.yml" <<EOF
name: homeassistant-core-systemd
deb:
  compression: zstd
section: misc
license: MIT
arch: all
version: ${tag}~ubuntu${DISTRIB_RELEASE}
version_schema: none
maintainer: "Stefan Heitmüller <stefan.heitmueller@gmx.com>"
description: Home Assistant Core - SystemD Service Unit
homepage: https://gitlab.com/packaging/homeassistant-core
depends:
  - ca-certificates
  - homeassistant-core ( = ${tag}~${DISTRIB_ID,,}${DISTRIB_RELEASE} )
contents:
  - src: ${tmpdir}/homeassistant-core.service
    dst: /lib/systemd/system/homeassistant-core.service
    file_info:
      mode: 0644
  - src: ${script_dir}/packaging/homeassistant-core-systemd/homeassistant-core.env
    dst: /etc/default/homeassistant-core
    file_info:
      mode: 0640
      owner: homeassistant
      group: homeassistant
scripts:
  postinstall: ${script_dir}/packaging/homeassistant-core-systemd/postinstall.sh
EOF
nfpm package --config "${tmpdir}/homeassistant-core-systemd.yml" --packager deb
for deb in ./*"${tag}~ubuntu${DISTRIB_RELEASE}"*.deb; do
    info "--- ${deb##*/}"
    dpkg -I "${deb}"
done

owner="$(stat -c %u "${script_dir}"/package.sh)"
group="$(stat -c %g "${script_dir}"/package.sh)"
install -o "${owner}" -g "${group}" -m 640 ./*"${tag}~ubuntu${DISTRIB_RELEASE}"*.deb "${script_dir}/${CI_JOB_NAME:-${DISTRIB_CODENAME}}/"

debug "Cache cleanup"
rm -rf \
    "${UV_CACHE_DIR}/archive-v0" \
    "${cache_dir}"/apt/*.deb
find \
    "${script_dir}"/package-*-* \
    -type f \
    \( -name "*.deb" -a ! -name "*${CI_COMMIT_TAG}*.deb" \) \
    -delete || true
chown -R "${owner}":"${group}" "${cache_dir}"
