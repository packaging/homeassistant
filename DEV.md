```bash
DOCKER_DEFAULT_PLATFORM=linux/amd64 docker run --rm -it -v $PWD:/source -e CI_PROJECT_DIR=/source -e CI_COMMIT_TAG=2024.10.3+1 -e DEBUG=1 --tmpfs /tmp:exec -w /tmp registry.gitlab.com/packaging/homeassistant-core/builder:noble
/source/package.sh
```
